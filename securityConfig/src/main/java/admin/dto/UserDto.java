package admin.dto;

import lombok.Data;

import javax.persistence.Id;

@Data
public class UserDto {
    @Id
    private Long id;
    private String name;
    private String surName;
    private String email;
    private String password;
}
