package admin.dto;

import lombok.Data;

import javax.persistence.Id;
@Data
public class UserAdminDto {
    @Id
    private Long id;
    private String name;
    private String surName;
    private String email;
    private String password;
    private String role;
}
