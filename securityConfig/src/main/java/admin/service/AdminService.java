package admin.service;

import admin.dto.UserAdminDto;
import admin.dto.UserDto;
import admin.model.UserEntity;
import admin.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AdminService {
    private final ModelMapper mapper;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserAdminDto upDateUSer(Long id, UserAdminDto dto) {
        dto.setPassword(encoder.encode(dto.getPassword()));
        UserEntity user = userRepository.findById(id).orElseThrow(RuntimeException::new);
        dto.setId(user.getId());
        UserEntity save = userRepository.save(mapper.map(dto, UserEntity.class));
        return mapper.map(save, UserAdminDto.class);
    }
}
