package admin.service;

import admin.dto.UserDto;
import admin.model.UserEntity;
import admin.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder encoder;

    public UserDto getUserId(Long id) {
        return userRepository.findById(id).map((s) -> mapper.map(s, UserDto.class)).
                orElseThrow(RuntimeException::new);
    }

    public UserDto RegisterUser(UserDto dto) {
        dto.setPassword(encoder.encode(dto.getPassword()));
        UserEntity user = mapper.map(dto, UserEntity.class);
        UserEntity save = userRepository.save(user);
        return mapper.map(save, UserDto.class);
    }

    public void deletedUser(Long id) {
        userRepository.deleteById(id);
    }

    public UserDto upDateUSer(Long id, UserDto dto) {
        dto.setPassword(encoder.encode(dto.getPassword()));
        UserEntity user = userRepository.findById(id).orElseThrow(RuntimeException::new);
        dto.setId(user.getId());
        UserEntity save = userRepository.save(mapper.map(dto, UserEntity.class));
        return mapper.map(save, UserDto.class);
    }
}
