package admin.controller;

import admin.dto.UserDto;
import admin.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {
    private final UserService userEntityService;

    @GetMapping("/public")
    public String getString(){
        return "Hello";
    }

    @GetMapping("{id}")
    public UserDto getUser(@PathVariable Long id) {
        return userEntityService.getUserId(id);
    }
    @PostMapping("/register")
    public  UserDto createUser(@RequestBody UserDto dto){
        return userEntityService.RegisterUser(dto);
    }
    @DeleteMapping("{id}")
    public void deleteUser(@PathVariable Long id) {
        userEntityService.deletedUser(id);
    }
    @PutMapping("{id}")
    public UserDto userUpdate(@PathVariable Long id, @RequestBody UserDto dto){
        return userEntityService.upDateUSer(id, dto);
    }
}
