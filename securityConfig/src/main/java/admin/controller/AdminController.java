package admin.controller;

import admin.dto.UserAdminDto;
import admin.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("admin")
@RequiredArgsConstructor
public class AdminController {
    private final AdminService adminService;

    @PutMapping("{id}")
    public UserAdminDto updateUser(@PathVariable Long id, @RequestBody UserAdminDto dto){
        return adminService.upDateUSer(id, dto);
    }


}
